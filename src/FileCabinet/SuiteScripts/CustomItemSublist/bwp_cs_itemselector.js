var CURRENTRECORDMODULE, SEARCHMODULE, TRANSLATIONMODULE, UIDIALOGMODULE, URLMODULE;

/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/currentRecord', 'N/search', 'N/translation', 'N/ui/dialog', 'N/url'], runClient);

function runClient(currentRecord, search, translation, uidialog, url) {
	CURRENTRECORDMODULE= currentRecord;
	SEARCHMODULE= search;
	TRANSLATIONMODULE= translation;
	UIDIALOGMODULE= uidialog;
	URLMODULE= url;
	
//	alert('runClient');
	
//	var table = document.getElementById('div__bodytab');
//	for (var i = 0 ; i < table.rows.length ; i++) {
//		table.rows[i].onclick = function() { rowClick(i); };
//	}
	
	var returnObj = {};
	returnObj['pageInit'] = _pageInit;
	returnObj['fieldChanged'] = _fieldChanged;
//	returnObj['postSourcing'] = _postSourcing;
//	returnObj['sublistChanged'] = _sublistChanged;
//	returnObj['lineInit'] = _lineInit;
//	returnObj['validateField'] = _validateField;
//	returnObj['validateLine'] = _validateLine;
//	returnObj['validateInsert'] = _validateInsert;
//	returnObj['validateDelete'] = _validateDelete;
//	returnObj['saveRecord'] = _saveRecord;
	returnObj['searchItems'] = searchItemsCall;
	return returnObj;
}
    
/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext) {
//	alert('item selector');
	window.onbeforeunload = null;
	var labelElement = window.document.getElementById('custpage_bwp_fld_wordsearch_fs_lbl_uir_label');
	labelElement.style.display='none';
	labelElement = window.document.getElementById('custpage_bwp_fld_pageselector_fs_lbl_uir_label');
	labelElement.style.display='none';
}

/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {
//	alert('_fieldChanged');
	// Navigate to selected page
	if (scriptContext.fieldId == 'custpage_bwp_fld_pageselector') {
		var pageId = scriptContext.currentRecord.getValue({
			fieldId : 'custpage_bwp_fld_pageselector'
		});	
		pageId = parseInt(pageId, 10);
		loadResultPage(scriptContext.currentRecord.getValue('custpage_bwp_fld_effectivewordsearch'), pageId);
	}
}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {

}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {

}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {
	alert('_lineInit');
}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {
	alert('_validateLine');
}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {

}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */
function _saveRecord(scriptContext) {
}

function searchItemsCall() {
	var currentRec = CURRENTRECORDMODULE.get();
	loadResultPage(currentRec.getValue('custpage_bwp_fld_wordsearch'), 0);
}

function getParameterFromURL(param) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		if (pair[0] == param) {
			return decodeURIComponent(pair[1]);
		}
	}
	return (false);
}

function loadResultPage(wordSearch, pageId) {
	var urlParams = {
		custparam_bwp_page: pageId,
		custparam_bwp_subsidiary: getParameterFromURL('custparam_bwp_subsidiary'),
		custparam_bwp_locations: getParameterFromURL('custparam_bwp_locations'),
		custparam_bwp_customer: getParameterFromURL('custparam_bwp_customer'),
		custparam_bwp_currency: getParameterFromURL('custparam_bwp_currency'),
		ifrmcntnr: 'T'
	};
	if (wordSearch) {
		urlParams.custparam_bwp_wordsearch = wordSearch;
	}	
	
	var pageUrl = URLMODULE.resolveScript({
		scriptId : getParameterFromURL('script'),
		deploymentId : getParameterFromURL('deploy'),
		params : urlParams
	});
	document.location.replace(pageUrl);	
}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
