var RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, UISERVERWIDGETMODULE, URLMODULE;
var PAGE_SIZE = 50;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/ui/serverWidget', 'N/url'], runSuitelet);

function runSuitelet(record, runtime, search, uiserverWidget, url) {
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= uiserverWidget;
	URLMODULE= url;
	
	var returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;	
}
   
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function _onRequest(context) {
    var method = context.request.method;
//    logRecord('context', context);

	log.debug(method);
    if (method == 'GET') {
    	getFunction(context);
    } else if (method == 'POST') {
    	postFunction(context);
    }
}

function getFunction(context) {
    var popupWindow = 'ifrmcntnr' in context.request.parameters;

    // Get parameters
	var wordSearch = context.request.parameters.custparam_bwp_wordsearch || '';
	var pageId = parseInt(context.request.parameters.custparam_bwp_page || 0, 10);
	var scriptId = context.request.parameters.script;
	var deploymentId = context.request.parameters.deploy;
	var subsidiary = context.request.parameters.custparam_bwp_subsidiary || '';
	var locations = context.request.parameters.custparam_bwp_locations || '';
	var customer = context.request.parameters.custparam_bwp_customer || '';
	var currency = context.request.parameters.custparam_bwp_currency || '';
	
	logVar('subsidiary', subsidiary);
	logVar('locations', locations);
	
	var form = getFormTemplate(subsidiary, locations, wordSearch, pageId, customer, currency);
	
	context.response.writePage(form);
}

function getFormTemplate(subsidiary, locations, wordSearch, pageId, customer, currency) {
    var itemSelectorForm = UISERVERWIDGETMODULE.createForm({
        title: 'Select item',
        hideNavBar: true
    });
    itemSelectorForm.clientScriptModulePath = './bwp_cs_itemselector.js';
    
    // Run search and determine page count
    var itemSearch = runItemSearch(PAGE_SIZE, subsidiary, locations, wordSearch);
    var pageCount = Math.ceil(itemSearch.count / PAGE_SIZE);

	// Set pageId to correct value if out of index
    if (pageCount == 0) {
    	pageId = -1;
    } else if (!pageId || pageId < 0) {
		pageId = 0;
	}
	else if (pageId >= pageCount) {
		pageId = pageCount - 1;
	}
    
	// **********************************************************
	// Form fields
	
	// Wor search field
    var wordSearchField = itemSelectorForm.addField({
    	id: 'custpage_bwp_fld_wordsearch',
    	label: 'aa',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    wordSearchField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
	});
    wordSearchField.defaultValue = wordSearch;

    var effectiveWordSearchField = itemSelectorForm.addField({
    	id: 'custpage_bwp_fld_effectivewordsearch',
    	label: 'aa',
    	type: UISERVERWIDGETMODULE.FieldType.TEXT
    });
    effectiveWordSearchField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
    effectiveWordSearchField.defaultValue = wordSearch;
	
	var searchButton = itemSelectorForm.addButton({
		id: 'custpage_bwp_btn_search',
		label: 'Search',
		functionName: 'searchItems'
	});

	// Page selector field
	var pageSelectorField = itemSelectorForm.addField({
		id: 'custpage_bwp_fld_pageselector',
		label: 'aa',
		type: UISERVERWIDGETMODULE.FieldType.SELECT
	});
	pageSelectorField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
	});
	pageSelectorField.updateBreakType({
    	breakType: UISERVERWIDGETMODULE.FieldBreakType.STARTROW
    });

	var pageRanges = itemSearch.pageRanges;
	for (var i = 0 ; i < pageCount ; i++) {
		var isSelected = i == pageId;
		pageSelectorField.addSelectOption({
			value: i,
			text: pageRanges[i].compoundLabel,
			isSelect: isSelected
		});
	}
	if (pageId >= 0) {
		pageSelectorField.defaultValue = pageId;		
	}
    
	// **********************************************************
	// Items sublist
	var itemSublist = itemSelectorForm.addSublist({
		id: 'custpage_bwp_list_items',
		label: 'items',
		type: UISERVERWIDGETMODULE.SublistType.LIST
	});
	
	var itemNameColumn = itemSublist.addField({
		id: 'custpage_bwp_col_itemname',
		type: UISERVERWIDGETMODULE.FieldType.TEXTAREA,
		label: 'Item'
	});
	itemNameColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.INLINE
	});
	var itemDescriptionColumn = itemSublist.addField({
		id: 'custpage_bwp_col_itemdescription',
		type: UISERVERWIDGETMODULE.FieldType.TEXTAREA,
		label: 'Description'    	
	});
	var itemDescriptionColumn = itemSublist.addField({
		id: 'custpage_bwp_col_price',
		type: UISERVERWIDGETMODULE.FieldType.TEXT,
		label: 'Price'    	
	});
	var itemDescriptionColumn = itemSublist.addField({
		id: 'custpage_bwp_col_available',
		type: UISERVERWIDGETMODULE.FieldType.FLOAT,
		label: 'Available'    	
	});
	
	// Load results form the active page
	var items = fetchSearchResult(itemSearch, pageId, customer, currency);
//	logRecord('items', items);
	var itemLine = 0;
	items.forEach(item => addItemToSublist(itemLine++, item, itemSublist));
//	addItemToSublist(itemLine++, {id: 5664, name: 'TESTEMO', displayName: ''}, itemSublist);
	
    return itemSelectorForm;
}

function addItemToSublist(itemLine, item, itemSublist) {
//	logVar('itemLine', itemLine);
//	logRecord('item', item);
	itemSublist.setSublistValue({
		id: 'custpage_bwp_col_itemname',
		line: itemLine,
		value: generateItemHtml(item.id, item.name + ' ' + item.displayName)
	});
	if (item.description) {
	    itemSublist.setSublistValue({
			id: 'custpage_bwp_col_itemdescription',
			line: itemLine,
			value: item.description
		});		
	}
	if (item.price) {
	    itemSublist.setSublistValue({
			id: 'custpage_bwp_col_price',
			line: itemLine,
			value: item.price
		});		
	}
	if (item.available) {
	    itemSublist.setSublistValue({
			id: 'custpage_bwp_col_available',
			line: itemLine,
			value: item.available
		});		
	}
}

function runItemSearchOld(searchPageSize, subsidiary, locations, wordSearch) {
	var searchFilters = [
		[
			["custitem_bwk_can_be_sold", "is", "T"], 
//			"OR", 
//			["type", "anyof", "Discount"]
		],
		"AND",
		["subsidiary", "is", subsidiary]
	];
	if (wordSearch) {
		searchFilters.push("AND", [["name", "startswith", wordSearch], "OR", ["displayname", "startswith", wordSearch]])
	}
	var searchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.ITEM,
		columns: ['internalid', 'name', 'displayname', 'description'],
  		filters: searchFilters
	});
	return searchObj.runPaged({
		pageSize : searchPageSize
	});
}

function runItemSearch(searchPageSize, subsidiary, locations, wordSearch) {
	if (!subsidiary) { return; }
	
	var searchFilters = [
		["custitem_bwk_can_be_sold","is","T"], 
		"AND",
		["subsidiary", "anyof", subsidiary],
		"AND",
		["isinactive","is","F"]
	];
	if (locations) {
		var locationFilter = [["formulanumeric: CASE WHEN (CASE WHEN to_char({type.id}) = 'Group' THEN {memberitem.inventorylocation.id} ELSE {inventorylocation.id} END) in (" + locations + ") THEN 1 ELSE  0 END", "equalto", 1],
		                      "OR",
		                      [["formulatext: to_char({type.id})", "is", 'Group'], "AND", ["inventorylocation","anyof","@NONE@"]],
		                      // No search operator allows to exclude a list of values ==> need to use a CASE formula
		                      "OR",
		                      [["formulanumeric: CASE WHEN to_char({type.id}) not in ('Assembly', 'Discount', 'InvtPart') THEN 1 ELSE 0 END", "equalto", 1], "AND", ["inventorylocation","anyof","@NONE@"]]
		];
		searchFilters.push('AND')
		searchFilters.push(locationFilter);		
	}
	if (wordSearch) {
		searchFilters.push("AND", [["name", "startswith", wordSearch], "OR", ["displayname", "startswith", wordSearch]])
	}
	
	var internalIdColumn = SEARCHMODULE.createColumn({
		name: "internalid",
		summary: "GROUP"
	});
	var nameColumn = SEARCHMODULE.createColumn({
		name: "name",
		summary: "GROUP",
		sort: SEARCHMODULE.Sort.ASC
	});
	var displayNameColumn = SEARCHMODULE.createColumn({
		name: "displayname",
		summary: "GROUP"
	});
	var descriptionColumn = SEARCHMODULE.createColumn({
		name: "description",
		summary: "GROUP"
	});
	var onHandQtyColumn = SEARCHMODULE.createColumn({
        name: "formulanumeric",
        summary: "MIN",
		formula: "CASE WHEN to_char({type.id}) = 'Group' THEN nvl({memberitem.locationquantityonhand},0) / {memberquantity} ELSE nvl({locationquantityonhand},0) END"
     });
	var itemSearchObj = SEARCHMODULE.create({
		   type: SEARCHMODULE.Type.ITEM,
		   filters: searchFilters,
		   columns: [
		      internalIdColumn, nameColumn, displayNameColumn, descriptionColumn, onHandQtyColumn
		   ]
	});
	return itemSearchObj.runPaged({
		pageSize : searchPageSize
	});
}

function fetchSearchResult(pagedData, pageIndex, customer, currency) {	
	var results = new Array();
	if (pageIndex >= 0) {		
		var searchPage = pagedData.fetch({
			index : pageIndex
		});	
		searchPage.data.forEach(function (result) {
			var displayName = result.getValue(result.columns[2]) || '';
			if (displayName == '- None -') {
				displayName = '';
			}
			var description = result.getValue(result.columns[3]) || '';
			if (description == '- None -') {
				description = '';
			}
			results.push({
				id: result.getValue(result.columns[0]) || '',
				name: result.getValue(result.columns[1]) || '',
				displayName: displayName,
				description: description,
				price: '',
				available: parseInt(result.getValue(result.columns[4]) || 0, 10)
			});
		});
		
		searchItemPrices(results, customer, currency);
	}
	
	return results;
}

function searchItemPrices(items, customer, currency) {
	logVar('items', items);
	logVar('currency', currency);
	var itemIds = [...new Set(items.map(x => x.id))];
	
	var itemTypeColumn = SEARCHMODULE.createColumn({
		name: "formulatext",
		formula: "{type.id}"
	});
	var itemAndGroupSearch = SEARCHMODULE.create({
	   type: SEARCHMODULE.Type.ITEM,
	   filters: [ ['internalid', 'anyof', itemIds] ],
	   columns: ['internalid', 'name', itemTypeColumn, 'memberitem', 'memberquantity']
	});

	var itemsComponents = [];
	var itemIdsSetForPriceSearch = new Set();
	itemAndGroupSearch.run().each(function(result) {
		var internalId = result.getValue({ name: 'internalid'});
		var itemType = result.getValue(itemTypeColumn);
		var memberItem = internalId;
		var memberQuantity = 1;
		
		if (itemType == 'Group') {
			memberItem = result.getValue({ name: 'memberitem' });
			memberName = result.getText({ name: 'memberitem' });
			memberQuantity = result.getValue({ name: 'memberquantity' });
		}
		var component = { itemId: memberItem, quantity: memberQuantity };
		
		var itemComponents = itemsComponents.find(x => x.itemId == internalId);
		if (!itemComponents) {
			itemsComponents.push({
				itemId: internalId,
				name: result.getValue({ name: 'name'}),
				components: [component]
			});
		} else {
			itemComponents.components.push(component);
		}
		itemIdsSetForPriceSearch.add(memberItem);
		return true;
	});
	
//	logRecord('itemsComponents', itemsComponents);
//	logRecord('itemIdsSetForPriceSearch', [...itemIdsSetForPriceSearch]);
	
	if (itemIdsSetForPriceSearch.size == 0 || !customer || !currency) { return; }
	
	// Search for customer price
	var unitPriceColumn = SEARCHMODULE.createColumn({
		name: 'unitprice',
        join: 'pricing'
	});
	var prices = [];
	var priceSearch = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.ITEM,
		filters:
		[
			["internalid", "anyof", [...itemIdsSetForPriceSearch]], 
			"AND", 
			["pricing.customer", "is", customer], 
			"AND", 
			["pricing.currency", "is", currency], 
			"AND", 
			[["type", "anyof", "Discount"], "OR", ["pricing.minimumquantity", "equalto", "0"]]
		],
		columns: ['internalid', unitPriceColumn]
	});
	priceSearch.run().each(function(result) {
		prices.push({ itemId: result.getValue({ name: 'internalid' }), price: result.getValue(unitPriceColumn)});
		return true;
	});	
//	logRecord('prices', prices);
	
	itemsComponents.forEach(function(itemComponents) {
		// Cumulate unitprice*quantity for each component of the item
		var price = itemComponents.components.reduce((a, b) => a + parseFloat((prices.find(x => x.itemId == b.itemId) || { price: 0 }).price) * parseFloat(b.quantity), 0);
//		logVar('price', price);
		items.find(x => x.id == itemComponents.itemId).price = price;
	});
	
	
	// Search for base price
	var basePrices = [];
	var basePriceSearch = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.ITEM,
		filters:
		[
			["internalid", "anyof", [...itemIdsSetForPriceSearch]], 
			"AND", 
			["pricing.pricelevel", "is", '1'], // id du base price - always 1 in all environments (SB, Prod)
			"AND", 
			["pricing.currency", "is", currency], 
			"AND", 
			[["type", "anyof", "Discount"], "OR", ["pricing.minimumquantity", "equalto", "0"]]
		],
		columns: ['internalid', unitPriceColumn]
	});
	basePriceSearch.run().each(function(result) {
		basePrices.push({ itemId: result.getValue({ name: 'internalid' }), price: result.getValue(unitPriceColumn)});
		return true;
	});
	
	itemsComponents.forEach(function(itemComponents) {
		// Populate price with base price in case price is empty
		var itemPrice = items.find(x => x.id == itemComponents.itemId).price;
		if (itemPrice) { return; }		
		
		// Cumulate unitprice*quantity for each component of the item
		var price = itemComponents.components.reduce((a, b) => a + parseFloat((basePrices.find(x => x.itemId == b.itemId) || { price: 0 }).price) * parseFloat(b.quantity), 0);
		items.find(x => x.id == itemComponents.itemId).price = price;
	});	
}

function generateItemHtml(itemId, itemName) {
	var itemHtml = `<span onclick="window.parent.require(['/SuiteScripts/CustomItemSublist/bwp_cs_customitemlist'], function(mod) { mod.setItemValue(${itemId}); }); closePopup(true);">${itemName}</span>`;
//	var itemHtml = `<span onclick="window.opener.require(['/SuiteScripts/DNA_Tests/bwp_cs_testemo'], function(mod) { mod.setItemValue(${itemId}); }); window.close();">${itemName}</span>`;
	
	return itemHtml;
}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
