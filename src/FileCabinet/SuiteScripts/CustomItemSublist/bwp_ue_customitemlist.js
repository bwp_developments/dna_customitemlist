var RECORDMODULE, SEARCHMODULE, UISERVERWIDGETMODULE;
var ALLOWEDRECORDTYPES= ['opportunity', 'estimate'];

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'N/ui/serverWidget'], runUserEvent);

function runUserEvent(record, search, serverWidget) {
	RECORDMODULE= record;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= serverWidget;
	
	var returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
//	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	if (!ALLOWEDRECORDTYPES.includes(scriptContext.newRecord.type)) { return; }
	log.debug('beforeLoad started');	
	
	if (scriptContext.request) {
		logRecord('request parameters', scriptContext.request.parameters);
		if (scriptContext.type != scriptContext.UserEventType.VIEW) {
			// Hide the standard item sublist
			var itemSublist = scriptContext.form.getSublist({ id: 'item' });
//			itemSublist.displayType = UISERVERWIDGETMODULE.SublistDisplayType.HIDDEN;		
		}
		
		// Add custom item sublist
		addCustomItemSublist(scriptContext);
		
		// Populate the custom sublist with data from Custom Item List record
		populateCustomItemSublist(scriptContext);		
	}
	
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {	
	if (!ALLOWEDRECORDTYPES.includes(scriptContext.newRecord.type)) { return; }
	
	log.debug('afterSubmit started');
	
	var transacRecord = scriptContext.newRecord;
//	logRecord('transacRecord', transacRecord);
	var documentId = transacRecord.id;
	var documentType = transacRecord.type;
	var customItemListRecords = searchCustomItemListRecords(documentId, documentType);
	// Save the custom item list
	var idsFromClient = [];
	var custItemLinesCount = transacRecord.getLineCount({ sublistId: 'custpage_bwp_sublist_item' }) || 0;
	for (var i = 0 ; i < custItemLinesCount ; i++) {
		var custItemLine = {
			internalId: transacRecord.getSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_internalid',
				line: i
			}),
			line: transacRecord.getSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_line',
				line: i
			}),
			displaySequence: i + 1,
			item: transacRecord.getSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_item',
				line: i
			}),
			itemDescription: transacRecord.getSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_itemdescription',
				line: i
			}),
			itemType: transacRecord.getSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_itemtype',
				line: i
			}),
			quantity: transacRecord.getSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_quantity',
				line: i
			}),
			itemRate: transacRecord.getSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_itemrate',
				line: i
			}),
			discountPercent: transacRecord.getSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_discountpercent',
				line: i
			}),
			amount: transacRecord.getSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_amount',
				line: i
			})
		};
		if (custItemLine.internalId) {
			idsFromClient.push(custItemLine.internalId);
		}
//		logRecord('custItemLine', custItemLine);
		
		saveCustomItemLine(custItemLine, documentId, documentType);
	}
	
	// Remove obsolete CustomItemList Records
	logRecord('customItemListRecords', customItemListRecords);
	deleteObsoleteCustomItemList(customItemListRecords, idsFromClient);
		
	log.debug('afterSubmit done');
}

/**
 * Save or update the record
 * @param custItemLine
 * @param customItemListRecords
 * @param documentId
 * @param documentType
 * @returns
 */
function saveCustomItemLine(custItemLine, documentId, documentType) {
	if (custItemLine.internalId) {
		RECORDMODULE.submitFields({
			type: 'customrecord_bwp_customitemlist',
			id: custItemLine.internalId,
			values: {
				custrecord_bwp_itemlistdocument: documentId,
				custrecord_bwp_itemlistdocumenttype: documentType,
				custrecord_bwp_itemlistline: custItemLine.line,
				custrecord_bwp_itemlistdisplaysequence: custItemLine.displaySequence,
				custrecord_bwp_itemlistitem: custItemLine.item,
				custrecord_bwp_itemlistitemtype: custItemLine.itemType,
				custrecord_bwp_itemlistquantity: custItemLine.quantity,
				custrecord_bwp_itemlistdescription: custItemLine.itemDescription,
				custrecord_bwp_itemlistdiscount: custItemLine.discountPercent,
				custrecord_bwp_itemlistrate: custItemLine.itemRate,
				custrecord_bwp_itemlistamount: custItemLine.amount
			}
		});
	} else {
		var recordObj = RECORDMODULE.create({
			type: 'customrecord_bwp_customitemlist'
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistdocument',
			value: documentId
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistdocumenttype',
			value: documentType
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistline',
			value: custItemLine.line
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistdisplaysequence',
			value: custItemLine.displaySequence
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistitem',
			value: custItemLine.item
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistitemtype',
			value: custItemLine.itemType
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistquantity',
			value: custItemLine.quantity
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistdescription',
			value: custItemLine.itemDescription
		});
		recordObj.setText({
			fieldId: 'custrecord_bwp_itemlistdiscount',
			text: custItemLine.discountPercent
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistrate',
			value: custItemLine.itemRate
		});
		recordObj.setValue({
			fieldId: 'custrecord_bwp_itemlistamount',
			value: custItemLine.amount
		});
		recordObj.save();		
	}
}

function deleteObsoleteCustomItemList(customItemListRecords, idsFromClient) {
//	logRecord('customItemListRecords', customItemListRecords);
//	logRecord('idsFromClient', idsFromClient);
	customItemListRecords.forEach(function(element) {
		logVar('element.internalId', element.internalId);
		if (!idsFromClient.includes(element.internalId)) {
			logVar('Delete this', element.internalId);
			RECORDMODULE.delete({
				type: 'customrecord_bwp_customitemlist',
				id: element.internalId
			});
		}
	});
}

function searchCustomItemListRecords(documentId, documentType) {
	var customItemListRecords = [];
//	logVar('documentId', documentId);
//	logVar('documentType', documentType);
	if (!documentId || !documentType) { return customItemListRecords; }
	
	var displaySequenceColumn = SEARCHMODULE.createColumn({
        name: "custrecord_bwp_itemlistdisplaysequence",
        sort: SEARCHMODULE.Sort.ASC
     });
	
	var searchObj = SEARCHMODULE.create({
		type: "customrecord_bwp_customitemlist",
		filters: [
			["custrecord_bwp_itemlistdocument", "equalto", documentId],
			"AND",
			["custrecord_bwp_itemlistdocumenttype", "is", documentType]
		],
		columns: [
			"internalid",
			"custrecord_bwp_itemlistdocument",
			"custrecord_bwp_itemlistdocumenttype",
			"custrecord_bwp_itemlistline",
			displaySequenceColumn,
			"custrecord_bwp_itemlistitem",
			"custrecord_bwp_itemlistitemtype",
			"custrecord_bwp_itemlistquantity",
			"custrecord_bwp_itemlistdescription",
			"custrecord_bwp_itemlistdiscount",
			"custrecord_bwp_itemlistrate",
			"custrecord_bwp_itemlistamount"
		]
	});
	
	searchObj.run().each(function(result){
//		logRecord('result', result);
		customItemListRecords.push({
			internalId: result.getValue({ name: 'internalid' }),
			document: result.getValue({ name: 'custrecord_bwp_itemlistdocument' }),
			documentType: result.getValue({ name: 'custrecord_bwp_itemlistdocumenttype' }),
			line: result.getValue({ name: 'custrecord_bwp_itemlistline' }),
			displaySequence: result.getValue(displaySequenceColumn),
			item: result.getValue({ name: 'custrecord_bwp_itemlistitem' }),
			itemType: result.getValue({ name: 'custrecord_bwp_itemlistitemtype' }),
			itemDescription: result.getValue({ name: 'custrecord_bwp_itemlistdescription' }),
			quantity: result.getValue({ name: 'custrecord_bwp_itemlistquantity' }),
			availableQty: 0,
			itemRate: result.getValue({ name: 'custrecord_bwp_itemlistrate' }),
			discountPercent: result.getValue({ name: 'custrecord_bwp_itemlistdiscount' }) || 0,
			amount: result.getValue({ name: 'custrecord_bwp_itemlistamount' }),
		});
		return true;
	});
	return customItemListRecords;
}

function addCustomItemSublist(scriptContext) {
	var formObject = scriptContext.form;
	formObject.clientScriptModulePath = './bwp_cs_customitemlist.js';
	
	var subsidiaryLocationsField = formObject.addField({
		id: 'custpage_bwp_fld_subsidiaryloc',
		label: 'subsidiary locations',
		type: UISERVERWIDGETMODULE.FieldType.TEXTAREA
	});
	subsidiaryLocationsField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
	
	var currentSublistLineField = formObject.addField({
		id: 'custpage_bwp_fld_currentsublistline',
		label: 'current line',
		type: UISERVERWIDGETMODULE.FieldType.TEXT
	});
	currentSublistLineField.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
	
	// Create the sublist
	var customItemSublist = formObject.addSublist({
		id: 'custpage_bwp_sublist_item',
		label: 'Summary',
		tab: 'items',
		type: UISERVERWIDGETMODULE.SublistType.INLINEEDITOR
	});
	
	// Internalid column
	var internalIdColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_internalid',
		label: 'internalid',
		type: UISERVERWIDGETMODULE.FieldType.TEXT
	});
	internalIdColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
	
	// Line column
	var lineColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_line',
		label: 'line',
		type: UISERVERWIDGETMODULE.FieldType.INTEGER
	});
	lineColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
	
	// Item column
	var itemColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_item',
		label: 'item',
		type: UISERVERWIDGETMODULE.FieldType.SELECT,
		source: 'item'
	});
	itemColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
	});
	
	// Item type column
	var itemTypeColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_itemtype',
		label: 'item type',
		type: UISERVERWIDGETMODULE.FieldType.TEXT
	});
	itemTypeColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.HIDDEN
	});
	
	// Quantity column
	var quantityColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_quantity',
		label: 'quantity',
		type: UISERVERWIDGETMODULE.FieldType.FLOAT
	});
	quantityColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
	});
	
	// Item description column
	var itemDescriptionColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_itemdescription',
		label: 'description',
		type: UISERVERWIDGETMODULE.FieldType.TEXTAREA
	});
	itemDescriptionColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.DISABLED
	});
	
	// Available quantity column
	var availableQtyColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_availableqty',
		label: 'available',
		type: UISERVERWIDGETMODULE.FieldType.FLOAT
	});
	availableQtyColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.DISABLED
	});
	
	// Discount percent column
	var discountColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_discountpercent',
		label: 'discount',
		type: UISERVERWIDGETMODULE.FieldType.PERCENT
	});
	discountColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
	});
	
	// Item rate column
	var rateColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_itemrate',
		label: 'rate',
		type: UISERVERWIDGETMODULE.FieldType.TEXT,
		// align: UISERVERWIDGETMODULE.LayoutJustification.RIGHT   this option is only available with list columns
	});
	rateColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.DISABLED // INLINE ==> hidden ; READONLY ==> not locked
	});
	// No way to change the justification of the rate field - on standard it is justified on right
	
	// Amount column
	var amountColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_amount',
		label: 'amount',
		type: UISERVERWIDGETMODULE.FieldType.FLOAT
	});
	amountColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.DISABLED
//		displayType: UISERVERWIDGETMODULE.FieldDisplayType.ENTRY
	});	
	// Amount column
	var amountDiscountedColumn = customItemSublist.addField({
		id: 'custpage_bwp_col_amountdiscounted',
		label: 'total',
		type: UISERVERWIDGETMODULE.FieldType.FLOAT
	});
	amountDiscountedColumn.updateDisplayType({
		displayType: UISERVERWIDGETMODULE.FieldDisplayType.DISABLED
	});	
}

function populateCustomItemSublist(scriptContext) {
	var documentId;
	var documentType;
	var clearCustomItemListId = false;
	if (scriptContext.newRecord.type == 'estimate' && scriptContext.type == scriptContext.UserEventType.CREATE) {
		// In case of an estimate created from an opportunity, load the Custom Item List records from the original opportunity
		var opportunity = scriptContext.newRecord.getValue({ fieldId: 'opportunity'});
		if (opportunity) {
			documentType = 'opportunity';
			documentId = opportunity;
			clearCustomItemListId = true;
		}		
	} else if (scriptContext.type == scriptContext.UserEventType.COPY) {
		var originalDocument = scriptContext.request.parameters.id;
		if (originalDocument) {
			documentType = scriptContext.newRecord.type;
			documentId = originalDocument;
			clearCustomItemListId = true;			
		}
	} else {
		documentId = scriptContext.newRecord.id;
		documentType = scriptContext.newRecord.type;		
	}
	var customItemListRecords = searchCustomItemListRecords(documentId, documentType);
	completeCustomItemListRecords(customItemListRecords, scriptContext);

	var customItemSublist = scriptContext.form.getSublist({
		id: 'custpage_bwp_sublist_item'
	});
	
	var line = 0; 
	customItemListRecords.forEach(element => addCustItemRecordToSublist(element, customItemSublist, line++, clearCustomItemListId));
}

function addCustItemRecordToSublist(custItemLine, customItemSublist, line, clearCustomItemListId) {
//	logRecord('custItemLine', custItemLine);
	if (clearCustomItemListId) {
		// The Custom Item Sublist is initialized with a copy of data coming from the apportunity
		// In this case leave blank the internalid: there is no customrecord_bwp_customitemlist record associated
	} else {
		customItemSublist.setSublistValue({
			id: 'custpage_bwp_col_internalid',
			line: line,
			value: custItemLine.internalId
		});
	}
	customItemSublist.setSublistValue({
		id: 'custpage_bwp_col_line',
		line: line,
		value: custItemLine.line
	});
	customItemSublist.setSublistValue({
		id: 'custpage_bwp_col_item',
		line: line,
		value: custItemLine.item
	});
	customItemSublist.setSublistValue({
		id: 'custpage_bwp_col_itemtype',
		line: line,
		value: custItemLine.itemType
	});
	customItemSublist.setSublistValue({
		id: 'custpage_bwp_col_quantity',
		line: line,
		value: custItemLine.quantity
	});
	if (custItemLine.itemDescription) {
		customItemSublist.setSublistValue({
			id: 'custpage_bwp_col_itemdescription',
			line: line,
			value: custItemLine.itemDescription
		});		
	}
	customItemSublist.setSublistValue({
		id: 'custpage_bwp_col_availableqty',
		line: line,
		value: custItemLine.availableQty
	});
	customItemSublist.setSublistValue({
		id: 'custpage_bwp_col_discountpercent',
		line: line,
		value: custItemLine.discountPercent
	});
	if (custItemLine.itemRate) {
		customItemSublist.setSublistValue({
			id: 'custpage_bwp_col_itemrate',
			line: line,
			value: custItemLine.itemRate
		});		
	}
	if (custItemLine.amount) {
		customItemSublist.setSublistValue({
			id: 'custpage_bwp_col_amount',
			line: line,
			value: custItemLine.amount
		});		
	}
	var amountDiscounted = custItemLine.amount * (100 - parseFloat(custItemLine.discountPercent)) / 100;
	amountDiscounted = amountDiscounted.toFixed(2);
//	logVar('amountDiscounted', amountDiscounted);
	customItemSublist.setSublistValue({
		id: 'custpage_bwp_col_amountdiscounted',
		line: line,
		value: amountDiscounted
	});
}

function completeCustomItemListRecords(customItemListRecords, scriptContext) {
	var items = [...new Set(customItemListRecords.map(x => parseInt(x.item, 10)))];
	if (!items || items.length == 0) { return; }
	var subsidiary = scriptContext.newRecord.getValue({ fieldId: 'subsidiary' });
	if (!subsidiary) { return; }
	
	searchSubsidiaryLocations(subsidiary);
	var itemsInfo = searchAvailableQty(
		items,
		subsidiary,
		scriptContext.newRecord.getValue({ fieldId: 'location' }) || searchSubsidiaryLocations(subsidiary)
	);
	customItemListRecords.forEach(function(element) {
		if (element.item in itemsInfo) {
			element.availableQty = itemsInfo[element.item].onHandQty;
		}
	});
}

function searchAvailableQty(itemIds, subsidiary, locations) {
	var itemsInfo = {};
	if (!subsidiary) { return itemsInfo; }
	if (!itemIds || itemIds.length == 0) { return itemsInfo; }
	
	var filters = [
		["internalid", "anyof", itemIds],
		"AND", 
		["subsidiary", "anyof", subsidiary]
	];
	if (locations) {
		var locationFilter = ["formulanumeric: CASE WHEN (CASE WHEN to_char({type.id}) = 'Group' THEN {memberitem.inventorylocation.id} ELSE {inventorylocation.id} END) in (" + locations + ") THEN 1 ELSE  0 END","equalto",1];
//		var locationFilter = [
//  	  		["formulanumeric: CASE WHEN (CASE WHEN to_char({type.id}) = 'Group' THEN {memberitem.inventorylocation.id} ELSE {inventorylocation.id} END) in (" + locations + ") THEN 1 ELSE  0 END","equalto",1],
//  			"OR",
//  			["inventorylocation", "anyof", "@NONE@"]
//      	];
		filters.push('AND');
		filters.push(locationFilter);		
	}
	var internalIdColumn = SEARCHMODULE.createColumn({
		name: "internalid",
		summary: "GROUP",
		sort: SEARCHMODULE.Sort.ASC
    });
	var onHandQtyColumn = SEARCHMODULE.createColumn({
		name: "formulanumeric",
		summary: "MIN",
		formula: "CASE WHEN to_char({type.id}) = 'Group' THEN nvl({memberitem.locationquantityonhand},0) / {memberquantity} ELSE nvl({locationquantityonhand},0) END"
    });
	var itemSearchObj = SEARCHMODULE.create({
	   type: SEARCHMODULE.Type.ITEM,
	   filters: filters,
	   columns:
	   [
	      internalIdColumn,
	      SEARCHMODULE.createColumn({
	         name: "type",
	         summary: "GROUP",
	         sort: SEARCHMODULE.Sort.ASC
	      }),
	      onHandQtyColumn
	   ]
	});
	itemSearchObj.run().each(function(result){
//		logRecord('result', result);
		itemsInfo[result.getValue(internalIdColumn)] = {
			onHandQty: parseInt(result.getValue(onHandQtyColumn), 10) || 0
		};
		return true;
	});
	return itemsInfo;
}

function searchSubsidiaryLocations(subsidiary) {
	var subsidiaryLocations = '';
	if (!subsidiary) { return subsidiaryLocations; }
	var searchObj = SEARCHMODULE.create({
		   type: SEARCHMODULE.Type.LOCATION,
		   filters: [
		      ["subsidiary", "is", subsidiary]
		   ],
		   columns: [
		      'internalid', 'name'
		   ]
		});
	searchObj.run().each(function(result){
//		logRecord('result', result);
		if (subsidiaryLocations) {
			subsidiaryLocations += ', ';
		}
		subsidiaryLocations += result.getValue('internalid');
		return true;
	});
	return subsidiaryLocations;
}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}