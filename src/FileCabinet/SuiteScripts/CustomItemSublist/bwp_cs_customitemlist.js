var CURRENTRECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, UIDIALOGMODULE, URLMODULE;
var ALLOWEDRECORDTYPES= ['opportunity', 'estimate'];

/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/currentRecord', 'N/runtime', 'N/search', 'N/ui/dialog', 'N/url'], runClient);

function runClient(currentRecord, runtime, search, uidialog, url) {
	CURRENTRECORDMODULE= currentRecord;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	UIDIALOGMODULE= uidialog;
	URLMODULE= url;
	
	var returnObj = {};
	returnObj['pageInit'] = _pageInit;
	returnObj['fieldChanged'] = _fieldChanged;
//	returnObj['postSourcing'] = _postSourcing;
//	returnObj['sublistChanged'] = _sublistChanged;
//	returnObj['lineInit'] = _lineInit;
	returnObj['validateField'] = _validateField;
	returnObj['validateLine'] = _validateLine;
//	returnObj['validateInsert'] = _validateInsert;
	returnObj['validateDelete'] = _validateDelete;
//	returnObj['saveRecord'] = _saveRecord;
	returnObj['displayItemSelector'] = displayItemSelectorCall;
	returnObj['displayItemSelectorModal'] = displayItemSelectorModalCall;
	returnObj['setItemValue'] = setItemValueCall;
	returnObj['clientAlert'] = clientAlertCall;
	return returnObj;
}
    
/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext) {
	if (!ALLOWEDRECORDTYPES.includes(scriptContext.currentRecord.type)) { return; }
	
	// Change the onclick code attached to item button
	var dropDownElement = document.getElementById('custpage_bwp_col_item_popup_muli');
	while (dropDownElement) {
//		dropDownElement.onclick = displayItemSelectorCall;
		dropDownElement.onclick = displayItemSelectorModalCall;
		dropDownElement = dropDownElement.nextElementSibling;
	}
	
	var subsidiary = scriptContext.currentRecord.getValue({ fieldId: 'subsidiary' });
	if (subsidiary) {
		scriptContext.currentRecord.setValue({
			fieldId: 'custpage_bwp_fld_subsidiaryloc',
			value: searchSubsidiaryLocations(subsidiary)
		});
	}
}

/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {
	if (!ALLOWEDRECORDTYPES.includes(scriptContext.currentRecord.type)) { return; }
	
//	alert('_fieldChanged ' + JSON.stringify(scriptContext));
//	var _fieldChanged = {
//		"currentRecord":{"id":"","isDynamic":true,"isNew":true,"isReadOnly":false,"type":"opportunity"},
//		"sublistId":"custpage_sublist_items",
//		"fieldId":"custpage_col_item",
//		"line":0
//	};
	if (scriptContext.sublistId == 'null' && scriptContext.fieldId == 'entity') {
		var subsidiary = scriptContext.currentRecord.getValue({ fieldId: 'subsidiary' });
		if (subsidiary) {
			scriptContext.currentRecord.setValue({
				fieldId: 'custpage_bwp_fld_subsidiaryloc',
				value: searchSubsidiaryLocations(subsidiary)
			});
		}
	}
	if (scriptContext.sublistId == 'custpage_bwp_sublist_item' && scriptContext.fieldId == 'custpage_bwp_col_item') {
		fieldChanged_itemCol(scriptContext);
	}
	
	if (scriptContext.sublistId == 'custpage_bwp_sublist_item' && scriptContext.fieldId == 'custpage_bwp_col_quantity') {
		fieldChanged_quantityCol(scriptContext);
	}
	
	if (scriptContext.sublistId == 'custpage_bwp_sublist_item' && scriptContext.fieldId == 'custpage_bwp_col_discountpercent') {
		fieldChanged_discountPercentCol(scriptContext);
	}
}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {

}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {

}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {

}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {
	if (!ALLOWEDRECORDTYPES.includes(scriptContext.currentRecord.type)) { return true; }
	
//	alert('_validateField ' + JSON.stringify(scriptContext));
	if (scriptContext.sublistId == 'custpage_bwp_sublist_item') {
		var currentLine = scriptContext.currentRecord.getValue({ fieldId: 'custpage_bwp_fld_currentsublistline'});
		if (currentLine != scriptContext.line) {
			scriptContext.currentRecord.setValue({ 
				fieldId: 'custpage_bwp_fld_currentsublistline',
				value: scriptContext.line
			});
		}
	}
	
	if (scriptContext.sublistId == 'custpage_bwp_sublist_item' && scriptContext.fieldId == 'custpage_bwp_col_item') {
		var itemId = scriptContext.currentRecord.getCurrentSublistValue({
			sublistId: scriptContext.sublistId,
			fieldId: scriptContext.fieldId
		});
		var itemType;
		if (itemId) {
			var fieldsValues = SEARCHMODULE.lookupFields({
				type: SEARCHMODULE.Type.ITEM,
				id: itemId,
				columns: ['type', 'custitem_bwk_can_be_sold']
			});
			var canBeSold = false;
			var itemType = '';
			if ('type' in fieldsValues) {
				if (fieldsValues.type.length >0) {
					itemType = fieldsValues.type[0].value;
				}
			}
			if ('custitem_bwk_can_be_sold' in fieldsValues) {
				canBeSold = fieldsValues.custitem_bwk_can_be_sold;
			}
//			if (!canBeSold && itemType != 'Discount') {
			if (!canBeSold) {
				scriptContext.currentRecord.setCurrentSublistValue({
					sublistId: scriptContext.sublistId,
					fieldId: 'custpage_bwp_col_item',
					value: ''
				});
				scriptContext.currentRecord.setCurrentSublistValue({
					sublistId: scriptContext.sublistId,
					fieldId: 'custpage_bwp_col_itemdescription',
					value: ''
				});
				UIDIALOGMODULE.alert({
					title: 'Invalid Item',
//					message: 'Only "Can Be Sold" items or Discount items are allowed'
					message: 'Only "Can Be Sold" items are allowed'
				});
				return false;
			}
		}
	}
	return true;
}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {
	if (!ALLOWEDRECORDTYPES.includes(scriptContext.currentRecord.type)) { return true; }
	
//	alert('_validateLine ' + JSON.stringify(scriptContext));
	
	if (scriptContext.sublistId == 'custpage_bwp_sublist_item') {
		var transacRecord = scriptContext.currentRecord;
		var itemAction = 'UPDATE';
		
		var custItemLine = {
			line: transacRecord.getCurrentSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_line'
			}),
			item: transacRecord.getCurrentSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_item'
			}),
			itemType: transacRecord.getCurrentSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_itemtype'
			}),
			itemDescription: transacRecord.getCurrentSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_itemdescription'
			}),
			quantity: transacRecord.getCurrentSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_quantity'
			}),
			itemRate: transacRecord.getCurrentSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_itemrate'
			}),
			discountPercent: transacRecord.getCurrentSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_discountpercent'
			})				
		};
//		alert(JSON.stringify(custItemLine));
		
		if (!custItemLine.item) { 
			return false;
		}
		
		if (!custItemLine.line) {
			itemAction = 'INSERT';
			var previousLine = parseInt(transacRecord.getValue({ fieldId: 'custbody_bwp_custitemlinenumber' }) || 0, 10);
			custItemLine.line = previousLine + 1;
			transacRecord.setCurrentSublistValue({
				sublistId: 'custpage_bwp_sublist_item',
				fieldId: 'custpage_bwp_col_line',
				value: custItemLine.line
			});
			transacRecord.setValue({
				fieldId: 'custbody_bwp_custitemlinenumber',
				value: custItemLine.line
			});
		}
//		alert(JSON.stringify(custItemLine));
		
		// Process item line
		var itemLineToInsert = -1;
		if (itemAction == 'INSERT') {
			// Insert a new item line at the right place: the sequence of lines in the item sublist must match the sequence from the custom sublist
			var currentSublistLine = transacRecord.getValue({ fieldId: 'custpage_bwp_fld_currentsublistline'});
			if (currentSublistLine == 0) {
				itemLineToInsert = 0;
			} else if (currentSublistLine < transacRecord.getLineCount({ sublistId: 'custpage_bwp_sublist_item'})) {
				var precedingLineValue = transacRecord.getSublistValue({
					sublistId: 'custpage_bwp_sublist_item',
					fieldId: 'custpage_bwp_col_line',
					line: currentSublistLine - 1
				});
				var discountLine = retrieveItemDiscountLine(scriptContext, precedingLineValue);
				itemLineToInsert = Math.max(discountLine.index, discountLine.endGroupIndex) +1;
			}
			
			// Create discount line first
			if (custItemLine.discountPercent && custItemLine.discountPercent > 0) {
				addDiscountLine(scriptContext, itemLineToInsert, custItemLine);
				if (itemLineToInsert < 0) {
					// The discount line has been added at the end of the item sublist, the item line must be inserted before the discount
					itemLineToInsert = transacRecord.getLineCount({ sublistId: 'item' }) - 1;					
				}
			}
			
			if (itemLineToInsert >= 0) {
				transacRecord.insertLine({
					sublistId: 'item',
					line: itemLineToInsert
				});
			} else {
				transacRecord.selectNewLine({ sublistId: 'item' });				
			}
		} else {
			// Update mode, find and select the line to update in the item sublist
			var itemLinesCount = transacRecord.getLineCount({ sublistId: 'item'});
			var isGroupDetected = false;
			var itemCustomLineNumber = 0;
			for (var i = itemLinesCount - 1 ; i >= 0 ; i--) {
				// 'Group' and 'EndGroup' lines does not register the value for the custom transaction line field
				var itemType = transacRecord.getSublistValue({
					sublistId: 'item',
					fieldId: 'itemtype',
					line: i
				});
				if (itemType == 'Discount') {
					continue;
				}
				if (itemType == 'EndGroup') {
					isGroupDetected = true;
					continue;
				}
				if (itemType != 'Group') {
					itemCustomLineNumber = transacRecord.getSublistValue({
						sublistId: 'item',
						fieldId: 'custcol_bwp_custitemlinenumber',
						line: i
					});
					if (isGroupDetected) {
						continue;
					}
				}
				if (itemCustomLineNumber == custItemLine.line) {
					transacRecord.selectLine({
						sublistId: 'item',
						line: i
					});
					break;
				}
				isGroupDetected = false;
				itemCustomLineNumber = 0;
			}
		}
		transacRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_custitemlinenumber',
			value: custItemLine.line
		});
		transacRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'item',
			value: custItemLine.item,
			fireSlavingSync: true,
//			forceSyncSourcing: true
		});
		transacRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'description',
			value: custItemLine.itemDescription
		});
		transacRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'quantity',
			value: custItemLine.quantity
		});
		
		// for testing purpose because VAT code should populate automatically
//		transacRecord.setCurrentSublistValue({
//			sublistId: 'item',
//			fieldId: 'taxcode',
//			value: '30'
//		});
		
		transacRecord.commitLine({ sublistId: 'item' });
		
		// Process discount line in  update mode
		if (itemAction == 'UPDATE') {
			var discountLine = {
				index: -1,
				endGroupIndex: -1				
			};
			discountLine = retrieveItemDiscountLine(scriptContext, custItemLine.line);
			
			if (custItemLine.discountPercent && custItemLine.discountPercent > 0) {
				if (discountLine.index >= 0) {
					transacRecord.selectLine({
						sublistId: 'item',
						line: discountLine.index
					});
				} else if (discountLine.endGroupIndex >= 0) {
					transacRecord.insertLine({
						sublistId: 'item',
						line: discountLine.endGroupIndex + 1
					});
				}
				populateDiscountLine(scriptContext, custItemLine);
			} else {
				// No discount, remove the discount line if it exists
				if (discountLine.index >= 0) {
					transacRecord.removeLine({
						sublistId: 'item',
						line: discountLine.index
					});
				}
			}
		}
	}
	return true;
}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {
	if (!ALLOWEDRECORDTYPES.includes(scriptContext.currentRecord.type)) { return true; }
	
	if (scriptContext.sublistId == 'custpage_bwp_sublist_item') {
		var transacRecord = scriptContext.currentRecord;
		
		var customLineNumber = transacRecord.getCurrentSublistValue({
			sublistId: 'custpage_bwp_sublist_item',
			fieldId: 'custpage_bwp_col_line'
		});
		var itemLinesCount = transacRecord.getLineCount({ sublistId: 'item'});
		var isGroupDetected = false;
		var customItemLineNumber = 0;
		for (var i = itemLinesCount - 1 ; i >= 0 ; i--) {
			// 'Group' and 'EndGroup' lines does not register the value for the custom transaction line field
			var itemType = transacRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'itemtype',
				line: i
			});
			if (itemType == 'EndGroup') {
				isGroupDetected = true;
				continue;
			}
			if (itemType != 'Group') {
				customItemLineNumber = transacRecord.getSublistValue({
					sublistId: 'item',
					fieldId: 'custcol_bwp_custitemlinenumber',
					line: i
				});
				if (isGroupDetected) {
					continue;
				}
			} else {
				isGroupDetected = false;
				itemCustomLineNumber = 0;				
			}
			if (customItemLineNumber == customLineNumber) {
				transacRecord.removeLine({
					sublistId: 'item',
					line: i
				});
			}
		}		
	}
	return true;
}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */
function _saveRecord(scriptContext) {
	
}

/**
 * Obsolete
 */
function displayItemSelectorCall() {	
	//Set window position values
	var leftPosition, topPosition;
	leftPosition = (window.screen.width / 2) - ((600 / 2) + 10);
	topPosition = (window.screen.height / 2) - ((600 / 2) + 50);

	//Define the window
	var params = 'height=' + 600 + ' , width=' + 600;
	params += ' , left=' + leftPosition + ", top=" + topPosition;
	params += ' ,screenX=' + leftPosition + ' ,screenY=' + topPosition;
	params += ', status=no'; 
	params += ' ,toolbar=no';
	params += ' ,menubar=no';
	params += ', resizable=yes';
	params += ' ,scrollbars=no';
	params += ' ,location=no';
	params += ' ,directories=no';
		
	//get the url for the suitelet
	var suiteletURL = URLMODULE.resolveScript({
		scriptId: 'customscript_bwp_sl_itemselector',
		deploymentId: 'customdeploy_bwp_sl_itemselector',
		params: {custparam_bwp_itemname: ''},
		returnExternalUrl: false
	});
	
	var scrollTop = document.documentElement.scrollTop;
//	alert('scrollTop ' + scrollTop);
	window.open(suiteletURL, "New Window Title", params);
	// After the window is opened the original window scrolls to the top because of NetSuite code
	// TODO: find a solution to this problem
}

function displayItemSelectorModalCall() {
	var currentRec = CURRENTRECORDMODULE.get();
	var subsidiary = currentRec.getValue('subsidiary');
	if (!subsidiary) { return; }
	
	//get the url for the suitelet
	var suiteletURL = URLMODULE.resolveScript({
		scriptId: 'customscript_bwp_sl_itemselector',
		deploymentId: 'customdeploy_bwp_sl_itemselector',
		params: {
			custparam_bwp_wordsearch: '',
			custparam_bwp_subsidiary: subsidiary,
			custparam_bwp_locations: currentRec.getValue('location') || currentRec.getValue('custpage_bwp_fld_subsidiaryloc'),
			custparam_bwp_customer: currentRec.getValue('entity'),
			custparam_bwp_currency: currentRec.getValue('currency')
		},
		returnExternalUrl: false
	});
	
//	nlExtOpenWindow(url, winname, width, height, fld, scrollbars, winTitle, listeners, triggerObj);
    var listeners = {};
//	nlExtOpenWindow(suiteletURL, "itemselector", 700, 700, this, true, '', listeners, this);
	nlExtOpenWindow(suiteletURL, "bwp_itemselector", 700, 700, this, true, '', listeners);
}

function addDiscountLine(scriptContext, linePosition, custItemLine) {
	var transacRecord = scriptContext.currentRecord;
	if (linePosition >= 0 && linePosition < transacRecord.getLineCount({ sublistId: 'item' })) {
		transacRecord.insertLine({
			sublistId: 'item',
			line: linePosition
		});
	} else {
		transacRecord.selectNewLine({ sublistId: 'item' });				
	}
	populateDiscountLine(scriptContext, custItemLine);
}

function populateDiscountLine(scriptContext, custItemLine) {
	var transacRecord = scriptContext.currentRecord;
	
	var scriptObject = RUNTIMEMODULE.getCurrentScript();
	var customDiscountItem = scriptObject.getParameter({ name: 'custscript_bwp_cs_customdiscountitem' });	
	
	transacRecord.setCurrentSublistValue({
		sublistId: 'item',
		fieldId: 'custcol_bwp_custitemlinenumber',
		value: custItemLine.line
	});
	// TODO: add a parameter for the internalid of the custom discount item ; value 5326 in SB1
	transacRecord.setCurrentSublistValue({
		sublistId: 'item',
		fieldId: 'item',
		value: customDiscountItem,
		fireSlavingSync: true,
//		forceSyncSourcing: true
	});
	transacRecord.setCurrentSublistValue({
		sublistId: 'item',
		fieldId: 'description',
		value: 'Custom discount'
	});
	transacRecord.setCurrentSublistValue({
		sublistId: 'item',
		fieldId: 'price',
		value: -1
	});
	var discountRate = '-' + custItemLine.discountPercent + '%';
	
	// Error
//	transacRecord.setCurrentSublistText({
//		sublistId: 'item',
//		fieldId: 'rate',
//		text: '-10,00%'
//	});
	/*
		An unexpected error occurred in a script running on this page.
		
		customscript_bwp_cs_testemo (validateLine)
		
		
		JS_EXCEPTION
		INVALID_FLD_VALUE Invalid number or percentage
	 */
	
	// Error
//	transacRecord.setCurrentSublistText({
//		sublistId: 'item',
//		fieldId: 'rate',
//		text: '-10.00%'
//	});
	/*
		 An unexpected error occurred in a script running on this page.

		customscript_bwp_cs_testemo (validateLine)
		
		
		JS_EXCEPTION
		INVALID_FLD_VALUE Invalid number or percentage
	 */
	
	// Error
//	transacRecord.setCurrentSublistValue({
//		sublistId: 'item',
//		fieldId: 'rate',
//		value: '-10.00%'
//	});
	/*
		An unexpected error occurred in a script running on this page.
		
		customscript_bwp_cs_testemo (validateLine)
		
		
		JS_EXCEPTION
		INVALID_FLD_VALUE Invalid number or percentage
	 */
	
	// Error
//	transacRecord.setCurrentSublistValue({
//		sublistId: 'item',
//		fieldId: 'rate',
//		value: '-10,00%'
//	});
	/*
		An unexpected error occurred in a script running on this page.
		
		customscript_bwp_cs_testemo (validateLine)
		
		
		JS_EXCEPTION
		INVALID_FLD_VALUE Invalid number or percentage
	 */
	
//	// trying to work with rate_formattedValue - a miracle is always possible
//	// ... but not, doesn't work
//	transacRecord.setCurrentSublistValue({
//		sublistId: 'item',
//		fieldId: 'rate_formattedValue',
//		value: discountRate			
//	});
//	
//	transacRecord.setCurrentSublistText({
//		sublistId: 'item',
//		fieldId: 'rate_formattedValue',
//		text: discountRate			
//	});
	
	// So the SuiteScript API is unabled to perform an update on rate field with a percentage
	// Finally the solution requires to bypass the SuiteScript API 
	document.forms['item_form'].elements['rate_formattedValue'].value = discountRate;
	document.forms['item_form'].elements['rate_formattedValue'].onchange();
//	window.document.getElementById('rate_formattedValue').value = discountRate;
//	window.document.getElementById('rate_formattedValue').onchange();
	
//	// for testing purpose because VAT code should populate automatically
//	transacRecord.setCurrentSublistValue({
//		sublistId: 'item',
//		fieldId: 'taxcode',
//		value: '30'
//	});
	
	transacRecord.commitLine({ sublistId: 'item' });
}

function setItemValueCall(itemId) {
	var recordObject = CURRENTRECORDMODULE.get();
	recordObject.setCurrentSublistValue({
		sublistId: 'custpage_bwp_sublist_item',
		fieldId: 'custpage_bwp_col_item',
		value: itemId
	});
}

function retrieveItemInfo(item, itemType, subsidiary, locations, customer, currency) {
	var itemInfo = {
		internalId: item,
		itemType: itemType,
		subsidiary: subsidiary,
		locations: locations,
		customer: customer,
		currency: currency,
		itemInfo: 0,
		unitPrice: 0
	};
	searchAvailableQty(itemInfo);
	searchItemPrice(itemInfo);
//	switch (itemType) {
//		case 'Group': 
//			searchGroupItemPrice(itemInfo);
//			break;
//		default:
//			searchRegularItemPrice(itemInfo);
//	}
	return itemInfo;
}

function searchAvailableQty(itemInfo) {
	if (!itemInfo.internalId || !itemInfo.subsidiary || !itemInfo.locations) { return; }
	
	var filters = [
		["internalid", "anyof", itemInfo.internalId],
		"AND", 
		["subsidiary", "anyof", itemInfo.subsidiary]
	];
	if (itemInfo.locations) {
		var locationFilter = ["formulanumeric: CASE WHEN (CASE WHEN to_char({type.id}) = 'Group' THEN {memberitem.inventorylocation.id} ELSE {inventorylocation.id} END) in (" 
		                      + itemInfo.locations + ") THEN 1 ELSE  0 END","equalto",1];
		filters.push('AND');
		filters.push(locationFilter);		
	}
	var internalIdColumn = SEARCHMODULE.createColumn({
		name: "internalid",
		summary: "GROUP",
		sort: SEARCHMODULE.Sort.ASC
    });
	var onHandQtyColumn = SEARCHMODULE.createColumn({
		name: "formulanumeric",
		summary: "MIN",
		formula: "CASE WHEN to_char({type.id}) = 'Group' THEN nvl({memberitem.locationquantityonhand},0) / {memberquantity} ELSE nvl({locationquantityonhand},0) END"
    });
	var itemSearchObj = SEARCHMODULE.create({
	   type: SEARCHMODULE.Type.ITEM,
	   filters: filters,
	   columns: [
	      internalIdColumn,
	      onHandQtyColumn
	   ]
	});
	itemSearchObj.run().each(function(result){
//		alert('result ' + JSON.stringify(result));
		itemInfo.onHandQty = parseInt(result.getValue(onHandQtyColumn), 10);
		return false;
	});
}

//function retrieveGroupItemInfo(item, itemType, customer, currency) {
//	var itemInfo = {
//		unitPrice: 0,
//		onHandQty: 0
//	};
//	var unitPriceColumn = SEARCHMODULE.createColumn({
//		name: "formulanumeric",
//		summary: "SUM",
//		formula: "{memberquantity} * {memberitem.price}"}
//	);
//	var availableQtyColumn = SEARCHMODULE.createColumn({
//		name: "formulanumeric",
//		summary: "MIN",
//		formula: "{memberitem.quantityonhand} / {memberquantity}"}
//	);
//	var itemgroupSearchObj = SEARCHMODULE.create({
//		type: "itemgroup",
//		filters: 
//		[
//			["internalid", "is", item]
//		],
//		columns:
//		[
//			SEARCHMODULE.createColumn({
//				name: "itemid",
//				summary: "GROUP",
//				sort: SEARCHMODULE.Sort.ASC
//			}), unitPriceColumn, availableQtyColumn
//		]
//	});
//	itemgroupSearchObj.run().each(function(result){
////		alert('result ' + JSON.stringify(result));
//		itemInfo.id = item;
//		itemInfo.unitPrice = result.getValue(unitPriceColumn);
//		itemInfo.onHandQty = parseInt(result.getValue(availableQtyColumn), 10);
//		return false;
//	});
//	return itemInfo;
//}

function searchGroupItemMembers(itemInfo) {
	if (!itemInfo.internalId || !itemInfo.customer || !itemInfo.currency) { return; }
	
	// list of memberitems internalids with quantity
	var groupMembers = [];
	var memberSearchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.ITEM,
		filters: 
		[
			['internalid', 'is', itemInfo.internalId]
		],
		columns: [ 'memberitem', 'memberquantity' ]
	});
	memberSearchObj.run().each(function(result){
//		alert('result ' + JSON.stringify(result));
		groupMembers.push({
			itemId: result.getValue({ name: 'memberitem'}),
			quantity: result.getValue({ name: 'memberquantity'})
		});
		return true;
	});
	
//	alert('memberIds ' + JSON.stringify(memberIds));
	return groupMembers;
	
//	var itemSearchObj = search.create({
//		   type: "item",
//		   filters:
//		   [
//		      ["internalid","anyof","6039"]
//		   ],
//		   columns:
//		   [
//		      search.createColumn({
//		         name: "itemid",
//		         sort: search.Sort.ASC
//		      }),
//		      "displayname",
//		      "salesdescription",
//		      "type",
//		      search.createColumn({
//		         name: "formulatext",
//		         formula: "{type.id}"
//		      }),
//		      search.createColumn({
//		         name: "internalid",
//		         join: "memberItem"
//		      })
//		   ]
//		});
//		var searchResultCount = itemSearchObj.runPaged().count;
//		log.debug("itemSearchObj result count",searchResultCount);
//		itemSearchObj.run().each(function(result){
//		   // .run().each has a limit of 4,000 results
//		   return true;
//		});
	
}

function searchItemPrice(itemInfo) {
	if (!itemInfo.internalId || !itemInfo.customer || !itemInfo.currency) { return; }
	
	var items = [];
	switch (itemInfo.itemType) {
		case 'Group': 
			items = searchGroupItemMembers(itemInfo);
			break;
		default:
			items.push({
				itemId: itemInfo.internalId,
				quantity: 1
			});
	}
	
	var itemIds = [];
	items.forEach(function(x) { if (!itemIds.includes(x.itemId)) { itemIds.push(x.itemId); }});	
	
	var unitPriceColumn = SEARCHMODULE.createColumn({
		name: 'unitprice',
        join: 'pricing'
	});
	var itemSearchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.ITEM,
		filters:
		[
		 	// Not supported on client script
//			["internalid", "anyof", [...new Set(items.map(x => x.itemIds))]], 
			["internalid", "anyof", itemIds],
			"AND", 
			["pricing.customer", "is", itemInfo.customer], 
			"AND", 
			["pricing.currency", "is", itemInfo.currency], 
			"AND", 
			[["type", "anyof", "Discount"], "OR", ["pricing.minimumquantity", "equalto", "0"]]
		],
		columns:
		[
		 	'internalid', 'type',
			SEARCHMODULE.createColumn({
				name: "currency",
				join: "pricing"
			}),
			SEARCHMODULE.createColumn({
				name: "customer",
				join: "pricing"
			}),
			unitPriceColumn
		]
	});
	itemInfo.unitPrice = 0;
	itemSearchObj.run().each(function(result){
//		alert('result ' + JSON.stringify(result));
		var internalId = result.getValue({ name: 'internalid'});
		var unitPrice = parseFloat(result.getValue(unitPriceColumn));
		// Not supported on client script
//		var quantity = parseInt(itemIds.find(x => x.itemId == internalId).quantity, 10);
		var quantity = parseInt(items.find(function(x) { return x.itemId == internalId; }).quantity, 10);
		itemInfo.unitPrice += parseFloat(unitPrice * quantity);
		return true;
	});
}

function clientAlertCall() {
	alert('alert client function testemo');
}

function fieldChanged_itemCol(scriptContext) {
	// Reset line fields 
	scriptContext.currentRecord.setCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_itemtype',
		value: ''
	});
	scriptContext.currentRecord.setCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_itemdescription',
		value: ''
	});
	scriptContext.currentRecord.setCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_discountpercent',
		value: ''
	});
	scriptContext.currentRecord.setCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_itemrate',
		value: ''
	});
	scriptContext.currentRecord.setCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_quantity',
		value: ''
	});
	scriptContext.currentRecord.setCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_amount',
		value: ''
	});
	scriptContext.currentRecord.setCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_amountdiscounted',
		value: ''
	});
	
	// Retrieve item informations and populate line fields
	var itemId = scriptContext.currentRecord.getCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: scriptContext.fieldId
	});
	if (itemId) {
		var fieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.ITEM,
			id: itemId,
			columns: ['type', 'description', 'baseprice']
		});
		
		var itemType = '';
		if ('type' in fieldsValues) {
			if (fieldsValues.type.length >0) {
				itemType = fieldsValues.type[0].value;
			}
		}

		scriptContext.currentRecord.setCurrentSublistValue({
			sublistId: scriptContext.sublistId,
			fieldId: 'custpage_bwp_col_itemtype',
			value: itemType
		});
		
		scriptContext.currentRecord.setCurrentSublistValue({
			sublistId: scriptContext.sublistId,
			fieldId: 'custpage_bwp_col_itemdescription',
			value: fieldsValues.description
		});
		
		var itemInfo = {};
		var amount;
			
		itemInfo = retrieveItemInfo(itemId, itemType, 
			scriptContext.currentRecord.getValue('subsidiary'),
			scriptContext.currentRecord.getValue({ fieldId: 'location' }) || scriptContext.currentRecord.getValue({ fieldId: 'custpage_bwp_fld_subsidiaryloc' }),
			scriptContext.currentRecord.getValue('entity'), 
			scriptContext.currentRecord.getValue('currency')
		);
		
		scriptContext.currentRecord.setCurrentSublistValue({
			sublistId: scriptContext.sublistId,
			fieldId: 'custpage_bwp_col_quantity',
			value: 1
		});

		scriptContext.currentRecord.setCurrentSublistValue({
			sublistId: scriptContext.sublistId,
			fieldId: 'custpage_bwp_col_itemrate',
			value: itemInfo.unitPrice || ''
		});
		scriptContext.currentRecord.setCurrentSublistValue({
			sublistId: scriptContext.sublistId,
			fieldId: 'custpage_bwp_col_availableqty',
			value: itemInfo.onHandQty || ''
		});
//		scriptContext.currentRecord.setCurrentSublistValue({
//			sublistId: scriptContext.sublistId,
//			fieldId: 'custpage_bwp_col_amount',
//			value: amount || ''
//		});
		updateLineAmounts(scriptContext);
	}
}

function fieldChanged_quantityCol(scriptContext) {
	updateLineAmounts(scriptContext);
}

function fieldChanged_discountPercentCol(scriptContext) {
	updateLineAmounts(scriptContext);
}

function updateLineAmounts(scriptContext) {	
	var unitPrice = scriptContext.currentRecord.getCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_itemrate'
	});
	var quantity = scriptContext.currentRecord.getCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_quantity'
	});
	var discountPercent = scriptContext.currentRecord.getCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_discountpercent'
	});
	var amount = unitPrice * quantity;
	var discountedAmount = amount * (100 - discountPercent) / 100;
	discountedAmount = discountedAmount.toFixed(2);
	
	scriptContext.currentRecord.setCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_amount',
		value: amount || ''
	});	
	scriptContext.currentRecord.setCurrentSublistValue({
		sublistId: scriptContext.sublistId,
		fieldId: 'custpage_bwp_col_amountdiscounted',
		value: discountedAmount || ''
	});
}

function retrieveItemDiscountLine(scriptContext, customItemLineNumber) {
	// Find the index of the discount line and the index of the item line that preceeds the discount line
	var transacRecord = scriptContext.currentRecord;
	var discountLine = {
		index: -1,
		endGroupIndex: -1				
	};
	var discountIndex = -1;
	var endGroupIndex = -1;
	var isGroupDetected = false;
	var itemCustomItemLineNumber = 0;
	var itemLinesCount = transacRecord.getLineCount({ sublistId: 'item'});
	for (var i = 0 ; i < itemLinesCount ; i++) {
		var itemType = transacRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'itemtype',
			line: i
		});
		
		if (itemType == 'Group') {
			isGroupDetected = true;
			continue;
		}
		if (itemType != 'EndGroup') {
			itemCustomItemLineNumber = transacRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_bwp_custitemlinenumber',
				line: i
			});
			// In case of item change, there is the 'Group' item but it is not yet expanded ==> no 'EndGroup' before the Discount item
//			if (isGroupDetected) {
			if (isGroupDetected && itemType != 'Discount') {
				continue;
			}
		}
		if (itemCustomItemLineNumber == customItemLineNumber) {
			if (itemType == 'Discount') {
				discountLine.index = i;
				break;
			} else {
				discountLine.endGroupIndex = i;
			}
		} else {
			if (discountLine.endGroupIndex > 0) {
				// the item lines related to custItemLine.line have already been read, don't read next item lines
				break;
			}			
		}
		isGroupDetected = false;
		itemCustomItemLineNumber = 0;
	}
	return discountLine;
}

/**
 * returns a string with locations separated by a comma
 */
function searchSubsidiaryLocations(subsidiary) {
	var subsidiaryLocations = '';
	if (!subsidiary) { return subsidiaryLocations; }
	var searchObj = SEARCHMODULE.create({
		   type: SEARCHMODULE.Type.LOCATION,
		   filters: [
		      ["subsidiary", "is", subsidiary]
		   ],
		   columns: [
		      'internalid', 'name'
		   ]
		});
	searchObj.run().each(function(result){
		logRecord('result', result);
		if (subsidiaryLocations) {
			subsidiaryLocations += ', ';
		}
		subsidiaryLocations += result.getValue('internalid');
		return true;
	});
	return subsidiaryLocations;
}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
